// TODO: Change search to regex to grab (?i) (use once_cell for regexes)
// TODO: Check if first char is upper or lower and change replacement accordingly
use anyhow::Result;
use dirs::home_dir;
use regex::Regex;
use serde_json::{json, Value};
use std::collections::BTreeMap;
use std::fmt::Write;
use std::fs;
use std::path::{Path, PathBuf};
use walkdir::WalkDir;

fn find_latest(index: &Path) -> PathBuf {
    let files = WalkDir::new(index)
        .min_depth(1)
        .into_iter()
        .filter_map(|file| file.ok())
        .filter(|file| file.metadata().unwrap().is_file())
        .last()
        .unwrap();
    files.path().to_owned()
}

fn to_hex_string(value: &str) -> String {
    let mut converted = String::new();
    for c in value.chars() {
        if (c as u32) > 127 {
            write!(converted, r"\u{:04x}", c as u32).unwrap();
        } else {
            converted.push(c);
        }
    }
    converted
}

fn get_translations(home_dir: &Path) -> Result<BTreeMap<String, Value>> {
    let index_file = home_dir.join(".minecraft/assets/indexes");
    let latest = find_latest(&index_file);
    let contents = fs::read_to_string(latest)?;
    let json: Value = serde_json::from_str(&contents)?;
    let fi_hash = json["objects"]["minecraft/lang/fi_fi.json"]["hash"]
        .as_str()
        .unwrap();
    let fi_head = &fi_hash[..2];
    let lang_file = home_dir
        .join(".minecraft/assets/objects")
        .join(fi_head)
        .join(fi_hash);

    println!("Finnish translation file: {}", lang_file.display());

    let string = fs::read_to_string(&lang_file)?;
    Ok(serde_json::from_str(&string)?)
}

fn write_to_file(home_dir: PathBuf, string: String) -> Result<()> {
    let resource_file =
        home_dir.join(".minecraft/resourcepacks/Suomi/assets/minecraft/lang/fi_fi.json");
    if !resource_file.is_file() {
        fs::create_dir_all(resource_file.parent().unwrap())?;
    }
    fs::write(resource_file, string)?;
    Ok(())
}

fn main() -> Result<()> {
    let home_dir = home_dir().expect("Unable to find home directory.");
    let mut map = get_translations(&home_dir)?;

    let mut trans = |from: &str, to: &str| {
        for i in 0..2 {
            let mut new_map = map.clone();
            for (k, v) in map.iter() {
                let mut v = v.as_str().unwrap().to_owned();
                if i == 1 {
                    v = v.replace(from, to);
                } else {
                    v = v.replace(&from.to_lowercase(), &to.to_lowercase());
                }
                new_map.insert(k.to_string(), json!(v));
            }
            map = new_map;
        }
    };

    trans("Nether-", "Horna");
    trans("Netherin", "Hornan");
    trans("Netheriitti", "Horniitti");
    trans("Netheristä", "Hornasta");
    trans("Netheriin", "Hornaan");
    trans("Nether", "Horna");

    trans("Shulkerin", "Kuoripiilijän");
    trans("Shulker", "Kuoripiilijä");

    trans("Endermite", "Ääripunkki");
    trans("Enderman", "Ääreläinen");
    trans("Ender-lohikäärme", "Äärilisko");
    trans("Ender-helmi", "Äärenhelmi");
    trans("Ender-arkku", "Ääriarkku");
    trans("Ender-silmä", "Äären silmä");
    trans("End-", "Ääri");

    trans("Witherin", "Näivettäjän");
    trans("Wither-ruusu", "Hornaruusu");
    trans("Wither-kallo", "Näivettäjän pääkallo");
    trans("Wither-luuranko", "Hornaluuranko");
    trans("Wither-luurangon kallo", "Hornaluurangon pääkallo");
    trans("Wither", "Näivettäjä");

    trans("Elytra", "Peitinsiivet");
    trans("Ghastin", "Hornanhengin");
    trans("Ghast", "Hornanhenki");

    trans("Redstone-", "Punakivi");
    trans("Redstonea", "Punakiveä");
    trans("Redstonelle", "Punakivelle");

    let mut string = serde_json::to_string_pretty(&map)?;
    string = to_hex_string(&string);
    let re_space = Regex::new(r"(?m)^\s{2}").unwrap();
    string = re_space.replace_all(&string, "    ").to_string();
    write_to_file(home_dir, string)?;

    Ok(())
}
